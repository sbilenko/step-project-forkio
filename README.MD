Проект Forkio виконувався без використання сторонніх бібліотек. Тільки препроцесор SCSS для написання стилів.
Проект збирався за допомогою Gulp.

Його авторами є: 
- Олександр Біленко, sbilenkooo@gmail.com 
- Руслан Кулагін, ruslankulahin@gmail.com.

Олександр Біленко виконував завдання №1: шапка сайту з випадаючим меню при малій роздільній здатності екрану, секція `People Are Talking About Fork`, налаштовував збірку Gulp.

Руслан Кулагін виконував завдання №2: блок `Revolutionary Editor`, секція `Here is what you get`, секція `Fork Subscription Pricing`, готував змінні та міксини.

GitHub Pages: 
sbilenko.github.io/Step-Project-Forkio/